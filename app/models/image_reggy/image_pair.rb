module ImageReggy
  class ImagePair < ActiveRecord::Base
    attr_accessible :initiator_id, :reference_id, :aligner, :reference, :initiator
    belongs_to :initiator, class_name: 'AlignmentImage', dependent: :destroy
    belongs_to :reference, class_name: 'AlignmentImage', dependent: :destroy
  end
end
