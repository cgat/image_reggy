module ImageReggy
  class AlignmentImage < ActiveRecord::Base
    extend CarrierWave::Meta::ActiveRecord

    #shutdown vips after commit (which is after processing has finished)
    #if the vips stuff gets moved to a background process (sidekiq), note
    #that this can be moved to a sidekiq middleware.
    #see this thread for more details https://github.com/jcupitt/ruby-vips/issues/55
    after_commit :vips_shutdown, on: :create
    def vips_shutdown
      ::VIPS::thread_shutdown if defined?(::VIPS)
    end

    attr_accessible :derivative_id, :derivative_type, :has_changed, :image, :reference

    has_one :image_pair, foreign_key: "initiator_id", dependent: :delete
    has_one :reference, through: :image_pair
    belongs_to :derivative, polymorphic: true
    acts_as_pointsable url_method: 'image_url'
    mount_uploader :image, AlignmentUploader
    serialize :image_meta, OpenStruct
    carrierwave_meta_composed :image_meta,
      :image, {image_large: [:width, :height]}, {image_web: [:width, :height]}

    def has_changed?
      has_changed
    end

    def fetch_image(version=:full)
      version = version.to_sym
      if version==:full
        self.image
      elsif valid_version?(version)
        self.image.send(version)
      end
    end

    def image_path(version=:full)
      fetch_image(version).file.path
    end

    def scaled_coords(version=:full)
      if self.image_width==0 || self.image_width.blank? || self.fetch_image(version).width==0 || self.fetch_image(version).width.blank?
        raise ArgumentError, "Width must be set alignment image"
      end
      scale = self.fetch_image(version).width/self.image.width.to_f
      points.map{|p| coords=p.to_coords; coords[0]=coords[0]*scale; coords[1]=coords[1]*scale; coords }
    end

    private

    def valid_version?(version)
      self.image.versions.keys.include?(version) or raise ArgumentError, "Invalid version, #{version}"
    end
  end
end
