(function() {
//classcompare functionality
$(window).load(function() {
    setup_classy_compare();
});

var setup_classy_compare = function() {
  $('.verify_before_after').ClassyCompare({
        leftgap: 10,
        rightgap: 10,
        caption: false,
        reveal: 0.5
    });
}
})();
