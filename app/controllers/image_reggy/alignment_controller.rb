module ImageReggy
  class AlignmentController < ImageReggy::ApplicationController
    include Wicked::Wizard

    steps :set_control_points, :verify_alignment

    def show
      @image_pair = ImagePair.find(params[:image_pair_id])
      @image_pair_form = ImagePairForm.new(@image_pair)
      case step
      when :set_control_points
        #the save is need to save the metadata
        @image_pair.initiator.image.recreate_versions!(:web) and @image_pair.initiator.save
        @image_pair.reference.image.recreate_versions!(:web) and @image_pair.reference.save
      when :verify_alignment
        @aligner = @image_pair_form.aligner.constantize.new(@image_pair, :web)
      end
      render_wizard
    end

    def update
      @image_pair = ImagePair.find(params[:image_pair_id])
      @image_pair_form = ImagePairForm.new(@image_pair.id)
      case step
      when :set_control_points
        if @image_pair_form.update(params)
          @aligner = @image_pair_form.aligner.constantize.new(@image_pair, :web)
          begin
            @aligner.align!
          rescue UnableToTransformError => e
            @image_pair_form.errors.add("Control Points", e.message)
            render_wizard and return
          end
          render_step(:verify_alignment) and return
        else
          render_wizard
        end
      when :verify_alignment
        aligner = @image_pair_form.aligner.constantize.new(@image_pair, :full)
        aligner.align!
        aligner.export(@image_pair)
        #this needs to be set before the image_pair is destroyed
        finish_path = finish_wizard_path
        #clean up the objects that should have been exported to main applications
        @image_pair.destroy
        redirect_to finish_path and return
      end
      render_wizard @image_pair
    end

    def finish_wizard_path
      derivative = @image_pair.initiator.derivative
      "/#{derivative.class.name.tableize}/#{derivative.id}"
    end

  end
end
