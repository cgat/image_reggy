module ImageReggy
  class ImagePairsController < ImageReggy::ApplicationController

    def create
      @image_pair_form = ImagePairForm.new
      if @image_pair_form.create(params)
        redirect_to image_reggy.image_pair_alignment_path(@image_pair_form.image_pair, :set_control_points)
      else
        render 'new'
      end
    end

    def destroy
      image_pair = ImagePair.find(params[:id])
      initiator_derivative = image_pair.initiator.derivative
      image_pair.destroy
      redirect_to main_app.url_for(initiator_derivative)
    end

  end
end
