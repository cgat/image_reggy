# encoding: utf-8
require 'carrierwave/processing/mime_types'

#Note on how why and how version names are renamed
#Why: when we convert to jpeg, we need to rename image file
#to use the proper jpeg extension. (in the case of tiffs).
#How: we are redefine the full_filename
#method. Note that normally you would want to call super
#in a similar way that this method is used in its
#original definition (in carrierwave version.rb). However,
#this does not actually overwrite the original method, but
#rather overrides it, and thus super calls the carrierwave
#defined method, which will double append the version
#name.

module ImageReggy
  class AlignmentUploader < CarrierWave::Uploader::Base
    include CarrierWave::Vips
    include CarrierWave::Meta
    #process :set_content_type
    process :store_meta

    # Choose what kind of storage to use for this uploader:
    storage :file
    #storage :fog

    # Override the directory where uploaded files will be stored.
    # This is a sensible default for uploaders that are meant to be mounted:
    def store_dir
      root = if Rails.env.test?
        "uploads/test"
      else
        "uploads"
      end
      "#{root}/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    end

    def move_to_store
      true
    end

    version :large do
      def full_filename(for_file)
        parent_name = for_file
        ext = File.extname(parent_name)
        base_name = parent_name.chomp(ext)
        [version_name, base_name].compact.join('_') + '.jpeg'
      end
      process :convert_to_8bit
      process resize_to_fit: [2000,2000]
      process convert: 'jpeg'
      process :store_meta
    end

    version :web, from_version: :large do
      def full_filename(for_file)
        parent_name = for_file
        ext = File.extname(parent_name)
        base_name = parent_name.chomp(ext)
        [version_name, base_name].compact.join('_') + '.jpeg'
      end
      process :convert_to_8bit
      process resize_to_fit: [900,900]
      process convert: 'jpeg'
      process :store_meta
    end

    def convert_to_8bit
      manipulate! do |image|
        #vips specific
        image = (image >> 8).clip2fmt :uchar if image.sizeof_element>1
        image
      end
    end

  end
end



