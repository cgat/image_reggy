module ImageReggy
  class ImagePairForm

    extend ActiveModel::Naming
    include ActiveModel::Conversion
    include ActiveModel::Validations

    attr_accessor :initiator, :reference, :image_pair, :aligner
    attr_accessor :initiator_derivative_class, :initiator_derivative_id, :reference_derivative_class, :reference_derivative_id

    validates :aligner, presence: true
    validate :presence_of_initiator, if: :new_record?
    validate :presence_of_reference, if: :new_record?
    validate :aligner_class_exists, if: :new_record?

    def persisted?
      false
    end

    def initialize(image_pair=nil)
      if !image_pair.present?
        return
      end

      if image_pair.class != ImageReggy::ImagePair
        image_pair = ImageReggy::ImagePair.find(image_pair)
      end

      self.image_pair = image_pair
      self.initiator = image_pair.initiator
      self.reference = image_pair.reference
      self.aligner = image_pair.aligner
    end

    #Used to update the points of the initiator and reference images
    def update(params)
      raise StandardError, "#update cannot be executed. Make sure to initialize object with image_pair_id" if self.image_pair.blank?
      initiator_params = params[:image_initiator]
      reference_params = params[:image_reference]
      self.initiator.assign_attributes(initiator_params)
      self.reference.assign_attributes(reference_params)
      if self.initiator.valid? && self.reference.valid?
        self.initiator.save!
        self.reference.save!
        true
      else
        false
      end
    end

    def create(params)
      self.aligner = params[:aligner]
      self.initiator_derivative_class = params[:initiator_derivative_class]
      self.initiator_derivative_id = params[:initiator_derivative_id]
      self.reference_derivative_class = params[:reference_derivative_class]
      self.reference_derivative_id = params[:reference_derivative_id]

      if valid?
        initiator_derivative = self.initiator_derivative_class.constantize.find_by_id(Integer(self.initiator_derivative_id))
        initiator = AlignmentImage.new(image: File.open(initiator_derivative.image.file.path))
        initiator.assign_attributes(params[:image_initiator])
        initiator.derivative = initiator_derivative
        reference_derivative = self.reference_derivative_class.constantize.find_by_id(Integer(self.reference_derivative_id))
        reference = AlignmentImage.new(image: File.open(reference_derivative.image.file.path))
        reference.assign_attributes(params[:image_reference])
        reference.derivative = reference_derivative
        image_pair = ImagePair.new(aligner: aligner)
        image_pair.initiator = initiator
        image_pair.reference = reference
        return false unless image_pair.save
        self.image_pair = image_pair
        self.initiator = image_pair.initiator
        self.reference = image_pair.reference
        true
      else
        false
      end
    end

    def presence_of_initiator
      presence_of_derivative("reference")
    end

    def presence_of_reference
      presence_of_derivative("initiator")
    end

    def presence_of_derivative(image_field_name)
      klass_field = "#{image_field_name}_derivative_class"
      id_field = "#{image_field_name}_derivative_id"
      if send(klass_field).blank? || send(id_field).blank?
        errors.add("#{image_field_name.capitalize} image", "can't be blank")
      end
    end

    def aligner_class_exists
      return if self.aligner.blank?
      #In development mode const_defined? won't work because the constant is lazily loaded
      #the statement below will load it if available
      self.aligner.constantize rescue
      unless Object.const_defined?(self.aligner)
        errors.add(:aligner, "The aligner is not defined: #{self.aligner}")
      end
    end

    def new_record?
      self.image_pair.blank?
    end

  end
end
