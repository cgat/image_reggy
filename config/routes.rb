ImageReggy::Engine.routes.draw do
  resources :image_pairs, only: [:create, :destroy] do
    resources :alignment, controller: 'alignment'
  end
end
