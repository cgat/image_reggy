# ImageReggy

This engine provides functionality to manually register images from a parent application using a set of corresponding points between two images. Currently the only transform supported is ScaleRotationTranslate (affine with uniform scaling). At the moment, it is assumed that the parent application uses carrierwave for store images. ImageReggy also requires use of a local file system.

## Current Feature Set And Limitations

-Only supports SRT (scale rotate translate) and full affine transforms
-Only works when parent application has access to the local filesystem

## Installation

### External Dependencies

ImageReggy relies on opencv and vips. These programs needed to be installed on your server. See [ruby-vips](https://github.com/jcupitt/ruby-vips) for notes on installing vips. Opencv can be installed on a Mac using `brew install opencv` (assuming you have homebrew installed). Also, see [ropencv](https://github.com/D-Alex/ropencv) for other installation notes (e.g. Linux).

### Install ImageReggy

ImageReggy is current not published, so you'll have to install through this git repository.

In Rails, add it to your Gemfile:

```ruby
gem 'pointsable', git: 'https://github.com/cgat/pointsable' #required until published
gem 'ropencv', git: 'https://github.com/D-Alex/ropencv' #required until version update
gem 'image_reggy', git: 'https://bitbucket.org/cgat/image_reggy'
```

## Migrating DB

Once the engine is installed into the parent application, ImageReggy tables need to be migrated into the parent application database.

    rake image_reggy:install:migrations
    rake db:migrate
    rake db:test:prepare

## How it Works

ImageReggy assumes that the parent application has two images that need to be registered. ImageReggy will create a copy of these images for the purpose of registration. The copied images are known as the initiator (the image that is initiating the process) and the reference (the image that is being referenced for registration). With the default behaviour, image registration is a two step process. In the first step, the user will set 4 control points between the two images. In the second step, the user is presented with a sample of the registration (with smaller images) via an overlay. If the user is satisfied with the registration, they can accept the verfication and the full sized images will be registered and replace the original images they were derived from.

## How to use ImageReggy in you App

### Create an Aligner

An Aligner is a class that defines how the registered is performed. It allows for relatively easy customization of the registration process.

Under the directoy `app/aligners/`, create a file called `<name>_aligner.rb` where `<name>` described the type of aligner you are creating.

The basic outline of an aligner will look like:

```ruby
class BasicAligner < ImageReggy::Aligner::Base
  include ImageReggy::ROpenCV
  transform_type :srt #currently only srt transforms are supported
  control_point_generator :manual #currently only manual control point generators are supported

  process :apply_transform #required in every aligner
  process :crop_to_fit #optional
end
```

### Initiate The Image Registration Process

The process of image registration is initiated by creating an ImageReggy::ImagePairForm and redirecting to ImageReggy's set control points form. To be able create an ImageReggy::ImagePairForm form, you need the following parameters defined:

-initiator_derviative_class
-initiator_deriviative_id
-reference_deriviative_class
-reference_deriviative_id
-aligner

where the class and id params are associated with the class and id of the image objects (ActiveRecord objects that have a carrierwave mounted column 'image') used as a basis for registration. The aligner parameter must be an upper case string of the aligner class you created earlier.

The simplest way to create ImageReggy::ImagePairForm object and redirect to the set control points form is to create a form with the above parameters and have it post to the ImageReggy::ImagePair controller. Alternatively, you could create a custom controller to handle the creation of the ImagePairForm (handy if you want to use a background job when creating ImagePairs). See below for any example:

```ruby
class MasterCreationController < ApplicationController

  #in this case, you would have to define a a template form master_creation/new.html.erb
  def new
    @image_pair_form = ImageReggy::ImagePairForm.new
  end

  def create
    @image_pair_form = ImageReggy::ImagePairForm.new
    if @image_pair_form.create(params[:image_pair_form])
      redirect_to image_reggy.image_pair_alignment_path(@image_pair_form.image_pair, :set_control_points)
    else
      render 'new'
    end
  end

end
```

## Customizing your Aligner

You can customize the alignment process by defining custom process methods in your aligner. You can also define how the registered images are exported back into your application (the default is to overwrite).

```ruby
class BasicAligner < ImageReggy::Aligner::Base
  include ImageReggy::ROpenCV
  transform_type :srt #currently only srt transforms are supported
  control_point_generator :manual #currently only manual control point generators are supported

  process :apply_transform #required in every aligner
  process :custom_method
  process :customize_transforms
  process :crop_to_fit #optional

  def custom_method
    manipulate_images! do |img1, img2, transform1, transform2|
      ... #manipulate using ropencv
      [img1, img2, transform1, transform2] #<= make sure to return everything at the end of the manipulate
    end
  end

  def customize_transforms
    #you can also use manipulate_transforms to make adjustments to the transforms only
    manipulate_transforms! do |transform1, transform2|
      ... #manipulate the transforms
      [transform1, transform2] #<= always return these values!
    end
  end

  #overwrite the export method to define how export will work.
  #this example behaviour is specific to some theotretical parent application
  def export(image_pair)
    initiator = image_pair.initiator
    reference = image_pair.reference
    initiator_derivative = initiator.derivative
    reference_derivative = reference.derivative
    # If you are defining your own export method, use the has_changed? method to see if the original image has changed since registeration
    if initiator.has_changed?
      parent = initiator_derivative.captureable
      parent.capture_images.create(image: File.open(initiator.image.file.path), image_state: "MASTER")
    end
    if reference.has_changed?
      parent = reference_derivative.captureable
      parent.capture_images.create(image: File.open(reference.image.file.path), image_state: "MASTER")
    end
    #return true if everything worked out
    true
  end

end
```


# License
This project is okay and uses MIT-LICENSE.
