class CreateImageReggyAlignmentImages < ActiveRecord::Migration
  def change
    create_table :image_reggy_alignment_images do |t|
      t.string :image
      t.string :derivative_id
      t.string :derivative_type
      t.boolean :has_changed

      t.timestamps
    end
  end
end
