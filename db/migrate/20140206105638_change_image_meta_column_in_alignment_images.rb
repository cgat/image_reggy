class ChangeImageMetaColumnInAlignmentImages < ActiveRecord::Migration
  def up
    change_column :image_reggy_alignment_images, :image_meta, :text
  end

  def down
    change_column :image_reggy_alignment_images, :image_meta, :string
  end
end
