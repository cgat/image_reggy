class AddImageMetaColumnToAlignmentImages < ActiveRecord::Migration
  def change
    add_column :image_reggy_alignment_images, :image_meta, :string
  end
end
