class RemoveWidthHeightColumns < ActiveRecord::Migration
  def up
    remove_column :image_reggy_alignment_images, :image_width
    remove_column :image_reggy_alignment_images, :image_height
  end

  def down
    add_column :image_reggy_alignment_images, :image_width, :integer
    add_column :image_reggy_alignment_images, :image_height, :integer
  end
end
