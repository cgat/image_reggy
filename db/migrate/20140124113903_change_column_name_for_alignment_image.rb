class ChangeColumnNameForAlignmentImage < ActiveRecord::Migration
  def up
    rename_column :image_reggy_alignment_images, :width, :image_width
    rename_column :image_reggy_alignment_images, :height, :image_height
  end

  def down
    rename_column :image_reggy_alignment_images, :image_width, :width
    rename_column :image_reggy_alignment_images, :image_height, :height
  end
end
