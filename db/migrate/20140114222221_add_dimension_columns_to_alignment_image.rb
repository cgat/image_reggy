class AddDimensionColumnsToAlignmentImage < ActiveRecord::Migration
  def change
    add_column :image_reggy_alignment_images, :width, :integer
    add_column :image_reggy_alignment_images, :height, :integer
  end
end
