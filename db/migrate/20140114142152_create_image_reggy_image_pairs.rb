class CreateImageReggyImagePairs < ActiveRecord::Migration
  def change
    create_table :image_reggy_image_pairs do |t|
      t.integer :initiator_id
      t.integer :reference_id

      t.timestamps
    end
  end
end
