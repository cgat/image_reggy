require 'spec_helper'

describe ImageReggy::Aligner do
  before(:each) do
    @aligner_class = Class.new(ImageReggy::Aligner::Base)
  end

  it 'should raise an error if the aligner class is initialized without an alignment processor' do
    image_pair = FactoryGirl.create(:image_pair)
    expect{ @aligner = @aligner_class.new(image_pair) }.to raise_error
  end

  describe ".process" do

    before(:each) do
      @aligner_class.send(:include, ImageReggy::ROpenCV)
      image_pair = FactoryGirl.create(:image_pair)
      @aligner = @aligner_class.new(image_pair)
      @aligner.stub(:transform!)
      @aligner.stub(:save_images!)
    end

    it "should add a single processor when a symbol is given" do
      @aligner_class.process :adjust_transform
      @aligner.should_receive(:adjust_transform)
      @aligner.align!
    end

    it "should add multiple processors when an array of symbols is given" do
      @aligner_class.process :adjust_transform, :apply_transform, :crop
      @aligner.should_receive(:adjust_transform)
      @aligner.should_receive(:apply_transform)
      @aligner.should_receive(:crop)
      @aligner.align!
    end

    it "should add a single processor with an argument when a hash is given" do
      @aligner_class.process :apply_transform => 'bicubic'
      @aligner.should_receive(:apply_transform).with('bicubic')
      @aligner.align!
    end

    it "should add a single processor with several argument when a hash is given" do
      @aligner_class.process :crop => [30, 30, 100, 200]
      @aligner.should_receive(:crop).with(30, 30, 100, 200)
      @aligner.align!
    end

    it "should add multiple processors when an hash with multiple keys is given" do
      @aligner_class.process :crop => [30, 30, 100, 200], :apply_transform => 'bicubic'
      @aligner.should_receive(:crop).with(30, 30, 100, 200)
      @aligner.should_receive(:apply_transform).with('bicubic')
      @aligner.align!
    end

    it "should call the processor if the condition method returns true" do
      @aligner_class.process :crop => [30, 30, 100, 200], :if => :true?
      @aligner_class.process :fancy, :if => :true?
      @aligner.should_receive(:true?).twice.and_return(true)
      @aligner.should_receive(:crop).with(30, 30, 100, 200)
      @aligner.should_receive(:fancy).with(no_args)
      @aligner.align!
    end

    it "should not call the processor if the condition method returns false" do
      @aligner_class.process :crop => [30, 30, 100, 200], :if => :false?
      @aligner_class.process :fancy, :if => :false?
      @aligner.should_receive(:false?).twice.and_return(false)
      @aligner.should_not_receive(:crop)
      @aligner.should_not_receive(:fancy)
      @aligner.align!
    end

  end

  describe '#align!' do

    before(:each) do
      @aligner_class.send(:include, ImageReggy::ROpenCV)
      image_pair = FactoryGirl.create(:image_pair)
      @aligner = @aligner_class.new(image_pair)
    end

    it 'should send transform! and save_images! messages on either side of processing' do
      @aligner.should_receive(:transform!)
      @aligner.should_receive(:save_images!)
      @aligner.should_receive(:save_metadata?).and_call_original
      @aligner.should_receive(:save_metadata).twice
      @aligner.align!
    end
  end

end
