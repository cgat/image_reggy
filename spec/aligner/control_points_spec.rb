require 'spec_helper'

describe ImageReggy::Aligner do

  before(:each) do
    @aligner_class = Class.new(ImageReggy::Aligner::Base)
    @aligner_class.send(:include, ImageReggy::ROpenCV)
    image_pair = FactoryGirl.create(:image_pair)
    @aligner = @aligner_class.new(image_pair)
  end

  describe '.control_points_method' do

    it 'should set the method to generate control points' do
      @aligner_class.control_point_generator :surf
      @aligner.should_receive(:surf)
      @aligner.set_control_points!
    end

    it 'should be set to manual by default' do
      expect(@aligner_class.control_point_generator).to eq(:manual)
    end

    it 'should not try to execute a manual method' do
      @aligner_class.control_point_generator :manual
      @aligner.should_not_receive(:manual)
      @aligner.set_control_points!
    end

    it 'should accept a hash and use the value of the as arguements to the method' do
      @aligner_class.control_point_generator :surf => [100,100]
      @aligner.should_receive(:surf).with(100,100)
      @aligner.set_control_points!
    end

    it 'should not accept a hash with more than one key/value pair' do
      expect{@aligner_class.control_point_generator :surf => [100,100], :illegal => [1]}.to raise_error(ArgumentError)
    end

  end

  describe '#set_control_points!' do

    context 'with :manual' do

      it 'should return false' do
        @aligner_class.control_point_generator(:manual)
        expect(@aligner.set_control_points!).to be_falsey
      end

    end

  end

end
