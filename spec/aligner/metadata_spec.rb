require 'spec_helper'

describe ImageReggy::Aligner do

  let(:image_with_metadata_path) do
    original = File.expand_path("spec/photos/color_8bit.tif")
    copy = original.sub("color","copy_color")
    FileUtils.cp(original, copy)
    copy
  end
  let(:image_with_metadata) { MiniExiftool.new(image_with_metadata_path) }

  let(:image_without_metadata_path) do
    original = File.expand_path("spec/photos/color_without_metadata.tif")
    copy = original.sub("color","copy_color")
    FileUtils.cp(original, copy)
    copy
  end
  let(:image_without_metadata) { MiniExiftool.new(image_without_metadata_path) }

  let(:aligner) do
    aligner_class = Class.new(ImageReggy::Aligner::Base)
    aligner_class.send(:include, ImageReggy::ROpenCV)
    image_pair = double('image_pair')
    image_pair.stub(:initiator).and_return(double('alignement_image'))
    image_pair.stub(:reference).and_return(double('alignement_image'))
    aligner_class.new(image_pair)
  end

  after(:each) do
    FileUtils.rm(Dir.glob(File.expand_path("spec/photos/copy_*")))
  end

  it 'image_without_metadata should have different metadata than image_with_metadata' do
    expect(image_without_metadata.make).to be_blank
    expect(image_with_metadata.make).to eq "Hasselblad"
  end

  describe '#save_metadata' do

    it 'should save the metadata from one image to another' do
      aligner.save_metadata(image_with_metadata_path, image_without_metadata_path)
      new_metadata = MiniExiftool.new(image_without_metadata_path)
      expect(new_metadata.make).to eq "Hasselblad"
    end

    context "when save_icc_profile? method returns true" do

      it 'should save the metadata from one image to another' do
        aligner.save_metadata(image_with_metadata_path, image_without_metadata_path)
        new_metadata = MiniExiftool.new(image_without_metadata_path)
        expect(new_metadata['ProfileCMMType']).to eq "ADBE"
      end

    end

    context "when save_icc_profile? method returns false" do

      it 'should save the metadata from one image to another' do
        aligner.stub(:save_icc_profile?) { false }
        aligner.save_metadata(image_with_metadata_path, image_without_metadata_path)
        new_metadata = MiniExiftool.new(image_without_metadata_path)
        expect(new_metadata['ProfileCMMType']).to_not eq "ADBE"
      end

    end

  end

end
