require 'spec_helper'

describe ImageReggy::Aligner::Base do

  before(:each) do
    @aligner_class = Class.new(ImageReggy::Aligner::Base)
  end

  describe ".transform_type" do

    it "should default to :srt" do
      expect(@aligner_class.transform_type).to eq(:srt)
    end

    it "should be able to be set without the equals operator" do
      @aligner_class.transform_type :perspective
      expect(@aligner_class.transform_type).to eq(:perspective)
    end

  end

end
