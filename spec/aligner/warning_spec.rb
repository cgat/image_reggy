require 'spec_helper'

describe ImageReggy::Aligner do
  before(:each) do
    @aligner_class = Class.new(ImageReggy::Aligner::Base)
    @aligner_class.send(:include, ImageReggy::ROpenCV)
    image_pair = double("'image pair", initiator: double("alignment image"), reference: double("alignment image"))
    @aligner = @aligner_class.new(image_pair)
  end

  describe '.warning' do

    it 'should return false when there are no warnings' do
      expect(@aligner.warning_messages?).to be_falsey
      expect(@aligner.warning_messages).to be_blank
    end

    it 'can handle multiple warnings' do
      @aligner_class.warning "This is a warning message1", if: Proc.new { true }
      @aligner_class.warning "This is a warning message2", if: Proc.new { true }
      expect(@aligner.warning_messages.count).to eq 2
    end

    it 'returns true if at least one warning is true' do
      @aligner_class.warning "This is a warning message1", if: Proc.new { true }
      @aligner_class.warning "This is a warning message2", if: Proc.new { false }
      expect(@aligner.warning_messages?).to be_truthy
    end

    context "conditional is a proc" do

      it 'should show the warning message if the condition is true' do
        @aligner_class.warning "This is a warning message", if: Proc.new { true }
        expect(@aligner.warning_messages?).to be_truthy
        expect(@aligner.warning_messages).to include("This is a warning message")
      end

      it 'should not show a warning messsage if the condition is false' do
        @aligner_class.warning "This is a warning message", if: Proc.new { false }
        expect(@aligner.warning_messages?).to be_falsey
        expect(@aligner.warning_messages).to be_blank
      end

    end

    context 'conditional is a symbol' do

      it 'should show the warning message if the condition is true' do
        @aligner.class_eval do
          def warning_method; true; end
        end
        @aligner_class.warning "This is a warning message", if: :warning_method
        expect(@aligner.warning_messages?).to be_truthy
        expect(@aligner.warning_messages).to include("This is a warning message")
      end

      it 'should not show a warning messsage if the condition is false' do
        @aligner.class_eval do
          def warning_method; false; end
        end
        @aligner_class.warning "This is a warning message", if: :warning_method
        expect(@aligner.warning_messages?).to be_falsey
        expect(@aligner.warning_messages).to be_blank
      end

    end

  end

end
