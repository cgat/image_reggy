require 'spec_helper'

describe ImageReggy::Aligner do

  before(:each) do
    @aligner_class = Class.new(ImageReggy::Aligner::Base)
    @aligner_class.send(:include, ImageReggy::ROpenCV)
    @image_pair = FactoryGirl.create(:image_pair)
    @aligner = @aligner_class.new(@image_pair)
  end

  describe '#export' do

    it 'should overwrite the reference derivative image when the image has been manipulated' do
      #replace the reference image to simulate a file manipulation
      FileUtils.cp(File.expand_path("spec/photos/image_reference_rot90.jpg"), @image_pair.reference.image_path(:full))
      @image_pair.reference.update_attribute(:has_changed, true)
      image_hash = hash_from_path(@image_pair.reference.image_path(:full))
      @aligner.export(@image_pair)
      expect(hash_from_path(@image_pair.reference.derivative.image.file.path)).to eq image_hash
    end

    it 'should recreate the versions of the reference derivative' do
      @image_pair.reference.update_attribute(:has_changed, true)
      @image_pair.reference.derivative.image.should_receive(:recreate_versions!)
      @aligner.export(@image_pair)
    end

    it 'should overwrite the initiator derivative image when the image has been manipulated' do
      #replace the reference image to simulate a file manipulation
      FileUtils.cp(File.expand_path("spec/photos/image_initiator_rot90.jpg"), @image_pair.initiator.image_path(:full))
      @image_pair.initiator.update_attribute(:has_changed, true)
      image_hash = hash_from_path(@image_pair.initiator.image_path(:full))
      @aligner.export(@image_pair)
      expect(hash_from_path(@image_pair.initiator.derivative.image.file.path)).to eq image_hash
    end

    it 'should recreate the versions of the initiator derivative' do
      @image_pair.initiator.update_attribute(:has_changed, true)
      @image_pair.initiator.derivative.image.should_receive(:recreate_versions!)
      @aligner.export(@image_pair)
    end

    describe 'overriding export in Aligner class for custom export' do

      it 'should user defined export' do
        @aligner_class.class_eval do
          def export(image_pair)
            "custom_export"
          end
        end
        @aligner = @aligner_class.new(@image_pair)
        expect(@aligner.export(@image_pair)).to eq "custom_export"
      end

    end

  end

end

def hash_from_path(path)
  Digest::MD5.hexdigest(File.read(path))
end
