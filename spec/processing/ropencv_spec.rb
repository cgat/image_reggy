require 'ropencv'
require 'spec_helper'
include OpenCV

describe ImageReggy::ROpenCV do
  before(:each) do
    @aligner_class = Class.new(ImageReggy::Aligner::Base)
    @aligner_class.transform_type :srt
    @aligner_class.send(:include, ImageReggy::ROpenCV)
    @initiator = double(ImageReggy::AlignmentImage)
    @reference = double(ImageReggy::AlignmentImage)
    @initiator.stub(:image_path).and_return(File.expand_path("spec/photos/image_initiator.jpg"))
    @reference.stub(:image_path).and_return(File.expand_path("spec/photos/image_reference.jpg"))
    image_pair = double(ImageReggy::ImagePair, initiator: @initiator, reference: @reference)
    @aligner = @aligner_class.new(image_pair)
    @aligner.instance_variable_set(:@cv_img_init, cv_img_init)
    @aligner.instance_variable_set(:@cv_img_ref, cv_img_ref)
  end

  let(:scale_half_mat) {
    #cv::Mat.new([[0.5, 0, 0], [0, 0.5, 0]])
    mat = cv::Mat.zeros(2, 3, cv::CV_64FC1)
    mat.set(0,0, 0.5)
    mat.set(1,1, 0.5)
    mat
  }
  let(:scale2x_mat) {
    #cv::Mat.new([[2, 0, 0], [0, 2, 0]])
    mat = cv::Mat.zeros(2, 3, cv::CV_64FC1)
    mat.set(0,0, 2)
    mat.set(1,1, 2)
    mat
  }
  let(:identity_mat) { cv::Mat.new([[1, 0, 0], [0, 1, 0]]) }
  let(:translate100_mat) { cv::Mat.new([[1, 0, 100], [0, 1, 100]]) }
  let(:cv_img_init) { cv::imread(@initiator.image_path) }
  let(:cv_img_ref) { cv::imread(@reference.image_path) }

  describe '#transform!' do

    it 'should raise an error when the points are on a line' do
      @initiator.stub(:scaled_coords).and_return([[100, 100], [200, 200], [300, 300]])
      @reference.stub(:scaled_coords).and_return([[200, 200], [400, 400], [600, 600]])
      expect{@aligner.transform!}.to raise_error
    end

    it 'should set the transform properly' do
      @initiator.stub(:scaled_coords).and_return([[300, 100], [200, 200], [300, 300]])
      @reference.stub(:scaled_coords).and_return([[600, 200], [400, 400], [600, 600]])
      @aligner.transform!
      expect(@aligner.t_reference.to_a.map{|row| row.map{|element| element.round(2)}}).to eq scale_half_mat.to_a
      expect(@aligner.t_initiator.to_a).to eq identity_mat.to_a
    end

    it 'should accept more than 3 points' do
      @initiator.stub(:scaled_coords).and_return([[300, 100], [200, 200], [300, 300], [400,200]])
      @reference.stub(:scaled_coords).and_return([[600, 200], [400, 400], [600, 600], [800,400]])
      @aligner.transform!
      expect(@aligner.t_reference.to_a.map{|row| row.map{|element| element.round(2)}}).to eq scale_half_mat.to_a
      expect(@aligner.t_initiator.to_a).to eq identity_mat.to_a
    end

    it 'should still work when one point is out of wack' do
      @initiator.stub(:scaled_coords).and_return([[300, 100], [200, 200], [300, 300], [400,200], [130, 21]])
      @reference.stub(:scaled_coords).and_return([[600, 200], [400, 400], [600, 600], [800,400], [33, 12]])
      @aligner.transform!
      expect(@aligner.t_reference.to_a.map{|row| row.map{|element| element.round(2)}}).to eq scale_half_mat.to_a
      expect(@aligner.t_initiator.to_a).to eq identity_mat.to_a
    end

  end

  describe '#apply_transform' do

    before(:each) do
      @aligner.instance_variable_set(:@t_initiator, scale_half_mat)
      @aligner.instance_variable_set(:@t_reference,scale2x_mat)
    end

    it 'should apply the currently set transforms to the image pair' do
      @aligner.apply_transform
      expect(hash_img(@aligner.cv_img_init)).to eq "baa08e066cc7d9ee26ec1502f07356aa"
      expect(hash_img(@aligner.cv_img_ref)).to eq  "3ac4fa52d062dcacf5397a463b1867a5"
    end

    it 'should resize the reference image to match the dimensions of the initiator' do
     match_size = [@aligner.cv_img_init.size.width,@aligner.cv_img_init.size.height]
     @aligner.apply_transform
     expect([@aligner.cv_img_ref.size.width,@aligner.cv_img_ref.size.height]).to eq (match_size)
     expect([@aligner.cv_img_init.size.width,@aligner.cv_img_init.size.height]).to eq (match_size)
    end

  end

  describe '#crop' do

    before(:each) do
      #crop to fit needs images of equal size. Both will now be 1000x1000 images (one black one white)
      @aligner.instance_variable_set(:@cv_img_ref, cv::Mat::ones(1000,1000, cv::CV_8UC3))
      @aligner.instance_variable_set(:@cv_img_init, cv::Mat::zeros(1000,1000, cv::CV_8UC3))
    end

    it 'should crop both images of the image_pair' do
      @aligner.crop(50,100, 500,600)
      expect(@aligner.cv_img_ref.size.width).to eq(500)
      expect(@aligner.cv_img_ref.size.height).to eq(600)
      expect(@aligner.cv_img_init.size.width).to eq(500)
      expect(@aligner.cv_img_init.size.height).to eq(600)
    end

  end

  describe "#crop_to_fit" do

    before(:each) do
      @aligner.instance_variable_set(:@t_initiator, identity_mat)
      @aligner.instance_variable_set(:@t_reference, scale_half_mat)
      #crop to fit needs images of equal size. Both will now be 1000x1000 images
      @aligner.instance_variable_set(:@cv_img_ref, cv::Mat::ones(1000,1000, cv::CV_8UC3))
      @aligner.instance_variable_set(:@cv_img_init, cv::Mat::zeros(1000,1000, cv::CV_8UC3))
      @original_size_ref = @aligner.cv_img_ref.size
      @aligner.instance_variable_set(:@ref_size_before_warp, @original_size_ref)
    end

    context "the reference is being scaled fully outside the bounds of the initiator" do

      before(:each) do
        @aligner.instance_variable_set(:@t_reference, scale2x_mat)
     end

      it 'should not apply any crop' do
        @aligner.crop_to_fit
        expect([@aligner.cv_img_ref.size.width, @aligner.cv_img_ref.size.height]).to eq [@original_size_ref.width, @original_size_ref.height]
        expect([@aligner.cv_img_init.size.width, @aligner.cv_img_init.size.height]).to eq [@original_size_ref.width, @original_size_ref.height]
      end

    end

    context "the reference is being scaled partially outside the bounds of the initiator" do

      before(:each) do
        @aligner.instance_variable_set(:@t_reference, translate100_mat)
      end

      it "should crop only the sides within the bounds" do
        @aligner.crop_to_fit
        expect([@aligner.cv_img_ref.size.width, @aligner.cv_img_ref.size.height]).to eq [@original_size_ref.width-100, @original_size_ref.height-100]
        expect([@aligner.cv_img_init.size.width, @aligner.cv_img_init.size.height]).to eq [@original_size_ref.width-100, @original_size_ref.height-100]
      end

    end

    context "the reference is being scaled within the bounds of the initiator" do

      it 'should crop the reference such that entire reference image is within the minimum bounding box' do
        @aligner.crop_to_fit
        expect([@aligner.cv_img_ref.size.width, @aligner.cv_img_ref.size.height]).to eq [@original_size_ref.width/2, @original_size_ref.height/2]
        expect([@aligner.cv_img_init.size.width, @aligner.cv_img_init.size.height]).to eq [@original_size_ref.width/2, @original_size_ref.height/2]
      end

      context "and the original reference size is bigger than the initiator size" do

        it 'should crop the reference such that entire reference image is within the minimum bounding box' do
          @original_size_ref = double('ref_size', width: 1500, height: 1500)
          @aligner.instance_variable_set(:@ref_size_before_warp, @original_size_ref)
          @aligner.crop_to_fit
          expect([@aligner.cv_img_ref.size.width, @aligner.cv_img_ref.size.height]).to eq [@original_size_ref.width/2, @original_size_ref.height/2]
          expect([@aligner.cv_img_init.size.width, @aligner.cv_img_init.size.height]).to eq [@original_size_ref.width/2, @original_size_ref.height/2]
        end

      end

    end

  end

  describe '#manipulate!' do

    before(:each) do
      @aligner.instance_variable_set(:@cv_img_ref, nil)
      @aligner.instance_variable_set(:@cv_img_init, nil)
    end

    it 'should load images once' do
      #one load for each image
      cv.should_receive(:imread).twice.and_call_original
      @aligner.manipulate! { |img_mat1, img_mat2, transform1, transform2| [img_mat1, img_mat2, transform1, transform2] }
      @aligner.manipulate! { |img_mat1, img_mat2, transform1, transform2| [img_mat1, img_mat2, transform1, transform2] }
      expect(@aligner.cv_img_ref).to be_present
      expect(@aligner.cv_img_init).to be_present
    end

  end

  describe '#save_images!' do

    before(:each) do
      @aligner.instance_variable_set(:@cv_img_ref, nil)
      @aligner.instance_variable_set(:@cv_img_init, nil)
    end

    describe 'idempotency of formats' do

      after(:each) do
        FileUtils.rm(Dir.glob("spec/photos/copy_*"))
      end

      %w{color_8bit.tif color_16bit.tif gray_8bit.tif gray_16bit.tif}.each do |filename|
        it 'should not modify the pixels after saving and reading' do
          @aligner.instance_variable_set(:@cv_img_init, cv::Mat::zeros(100,100, cv::CV_8UC3))
          FileUtils.cp("spec/photos/#{filename}","spec/photos/copy_#{filename}")
          #If we don't stub the image path here, save_images will overwrite photos in our spec folder
          @initiator.stub(:image_path).and_return(File.expand_path("spec/photos/copy_init_#{filename}"))
          @reference.stub(:image_path).and_return(File.expand_path("spec/photos/copy_#{filename}"))
          @aligner.manipulate! { |img_mat1, img_mat2, transform1, transform2| [img_mat1, img_mat2, transform1, transform2] }
          tmp_ref = @aligner.cv_img_ref.clone()
          @aligner.save_images!
          @aligner.instance_variable_set(:@cv_img_ref, nil)
          diff = cv::Mat.new()
          diff1color = cv::Mat.new()
          @aligner.manipulate! { |img_mat1, img_mat2, transform1, transform2| [img_mat1, img_mat2, transform1, transform2] }
          cv::compare(tmp_ref, @aligner.cv_img_ref, diff, cv::CMP_NE)
          expect(cv::countNonZero(diff1color)).to eq 0
        end
      end

    end

    describe '#mat_difference?' do

      it "returns true if matrices have differencet dimensions" do
        mat1 = cv::Mat::zeros(100,100,cv::CV_8UC3)
        mat2 = cv::Mat::zeros(200,100,cv::CV_8UC3)
        expect(@aligner.mat_difference?(mat1,mat2)).to be_truthy
      end

      it "returns true if matrices have different type" do
        mat1 = cv::Mat::zeros(100,100,cv::CV_8UC1)
        mat2 = cv::Mat::zeros(100,100,cv::CV_8UC3)
        expect(@aligner.mat_difference?(mat1,mat2)).to be_truthy
      end

      it "returns false if matrices different on one element on the first channel" do
        mat1 = cv::Mat::zeros(100,100,cv::CV_8UC3)
        mat1[99,99,0] = 1
        mat2 = cv::Mat::zeros(100,100,cv::CV_8UC3)
        expect(@aligner.mat_difference?(mat1,mat2)).to be_truthy
      end

      it "returns false if matrices different on one element on the second channel" do
        mat1 = cv::Mat::zeros(100,100,cv::CV_8UC3)
        mat1[99,99,1] = 1
        mat2 = cv::Mat::zeros(100,100,cv::CV_8UC3)
        expect(@aligner.mat_difference?(mat1,mat2)).to be_truthy
      end

      it "returns false if matrices have the same elements but in different order" do
        mat1 = cv::Mat::zeros(100,100,cv::CV_8UC3)
        mat1[99,99,0] = 1
        mat2 = cv::Mat::zeros(100,100,cv::CV_8UC3)
        mat2[0,0,0] = 1
        expect(@aligner.mat_difference?(mat1,mat2)).to be_truthy
      end

    end

end





end


