require 'spec_helper'

module ImageReggy
  describe AlignmentImage do
    describe "#reference" do
      let(:image1)  { FactoryGirl.create(:alignment_image) }
      let(:image2)  { FactoryGirl.create(:alignment_image) }
      before(:each) do
        image1.reference = image2
        image1.save!
      end
      it "can have a reference image" do
        expect(image1.reference.id).to eq(image2.id)
      end
      it "does not make a mutual relationship" do
        expect(image2.reference).to be_blank
      end
    end
  end
end
