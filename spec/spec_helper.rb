ENV['RAILS_ENV'] ||= 'test'
require File.expand_path("../dummy/config/environment.rb",  __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'factory_girl_rails'
require 'database_cleaner'
require 'capybara/firebug/rspec'

Rails.backtrace_cleaner.remove_silencers!

#Use this for chrome driver
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

#Use this if testing large images
# Capybara.register_driver :selenium_with_long_timeout do |app|
#   client = Selenium::WebDriver::Remote::Http::Default.new
#   client.timeout = 500
#   Capybara::Selenium::Driver.new(app, :browser => :firefox, :http_client => client)
# end
# Capybara.javascript_driver = :selenium_with_long_timeout

# Load support files
Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }
RSpec.configure do |config|
  config.mock_with :rspec
  config.use_transactional_fixtures = false
  config.infer_base_class_for_anonymous_controllers = false
  config.order = "random"
  config.after(:all) do
    # Get rid of the linked images
    if Rails.env.test?
      FileUtils.rm_rf(Dir["#{Rails.root}/public/uploads/test"])
    end
  end

  config.infer_spec_type_from_file_location!
end

def hash_img(img)
  # Compute a hash for an image, useful for image comparisons
  Digest::MD5.hexdigest(img.data.read_string)
end
