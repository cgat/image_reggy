class CreatePairs < ActiveRecord::Migration
  def change
    create_table :pairs do |t|
      t.integer :repeat_image_id
      t.integer :historic_image_id

      t.timestamps
    end
  end
end
