class CreateRepeatImages < ActiveRecord::Migration
  def change
    create_table :repeat_images do |t|
      t.string :image

      t.timestamps
    end
  end
end
