include ActionDispatch::TestProcess

def find_or_create_image(klass, path)
  p=Pathname.new(path)
  result = klass.where("image like ?",p.basename.to_s)
  if result.present?
    result.first
  else
    klass.create(image: fixture_file_upload(p.to_s))
  end
end

r1=find_or_create_image(RepeatImage, "/Users/chrisgat/projects/image_reggy/spec/photos/image_initiator.jpg")
h1=find_or_create_image(HistoricImage, "/Users/chrisgat/projects/image_reggy/spec/photos/image_reference.jpg")
r1.historic_image = h1
r1.save!
r2=find_or_create_image(RepeatImage, "/Users/chrisgat/projects/image_reggy/spec/photos/big_initiator.jpg")
h2=find_or_create_image(HistoricImage, "/Users/chrisgat/projects/image_reggy/spec/photos/big_reference.jpg")
r2.historic_image = h2
r2.save!



