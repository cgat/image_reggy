# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140206105638) do

  create_table "historic_images", force: true do |t|
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "image_reggy_alignment_images", force: true do |t|
    t.string   "image"
    t.string   "derivative_id"
    t.string   "derivative_type"
    t.boolean  "has_changed"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "image_meta",      limit: 255
  end

  create_table "image_reggy_image_pairs", force: true do |t|
    t.integer  "initiator_id"
    t.integer  "reference_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "aligner"
  end

  create_table "pairs", force: true do |t|
    t.integer  "repeat_image_id"
    t.integer  "historic_image_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pointsable_points", force: true do |t|
    t.integer  "x"
    t.integer  "y"
    t.string   "label"
    t.string   "pointsable_type"
    t.integer  "pointsable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "repeat_images", force: true do |t|
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
