class RepeatImage < ActiveRecord::Base
  has_one :pair
  has_one :historic_image, through: :pair
  attr_accessible :image, :historic_image
  mount_uploader :image, ImageUploader

end
