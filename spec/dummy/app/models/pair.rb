class Pair < ActiveRecord::Base
  attr_accessible :historic_image_id, :repeat_image_id, :historic_image, :repeat_image
  belongs_to :historic_image
  belongs_to :repeat_image
end
