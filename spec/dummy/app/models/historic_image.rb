class HistoricImage < ActiveRecord::Base
  has_one :pair
  has_one :repeat_image, through: :pair
  attr_accessible :image, :repeat_image
  mount_uploader :image, ImageUploader

end
