class HistoricImagesController < ApplicationController
  # GET /historic_images
  # GET /historic_images.json
  def index
    @historic_images = HistoricImage.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @historic_images }
    end
  end

  # GET /historic_images/1
  # GET /historic_images/1.json
  def show
    @historic_image = HistoricImage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @historic_image }
    end
  end

  # GET /historic_images/new
  # GET /historic_images/new.json
  def new
    @historic_image = HistoricImage.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @historic_image }
    end
  end

  # GET /historic_images/1/edit
  def edit
    @historic_image = HistoricImage.find(params[:id])
  end

  # POST /historic_images
  # POST /historic_images.json
  def create
    @historic_image = HistoricImage.new(params[:historic_image])

    respond_to do |format|
      if @historic_image.save
        format.html { redirect_to @historic_image, notice: 'Historic image was successfully created.' }
        format.json { render json: @historic_image, status: :created, location: @historic_image }
      else
        format.html { render action: "new" }
        format.json { render json: @historic_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /historic_images/1
  # PUT /historic_images/1.json
  def update
    @historic_image = HistoricImage.find(params[:id])

    respond_to do |format|
      if @historic_image.update_attributes(params[:historic_image])
        format.html { redirect_to @historic_image, notice: 'Historic image was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @historic_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /historic_images/1
  # DELETE /historic_images/1.json
  def destroy
    @historic_image = HistoricImage.find(params[:id])
    @historic_image.destroy

    respond_to do |format|
      format.html { redirect_to historic_images_url }
      format.json { head :no_content }
    end
  end
end
