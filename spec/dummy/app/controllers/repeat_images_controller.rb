class RepeatImagesController < ApplicationController
  # GET /repeat_images
  # GET /repeat_images.json
  def index
    @repeat_images = RepeatImage.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @repeat_images }
    end
  end

  # GET /repeat_images/1
  # GET /repeat_images/1.json
  def show
    @repeat_image = RepeatImage.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @repeat_image }
    end
  end

  # GET /repeat_images/new
  # GET /repeat_images/new.json
  def new
    @repeat_image = RepeatImage.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @repeat_image }
    end
  end

  # GET /repeat_images/1/edit
  def edit
    @repeat_image = RepeatImage.find(params[:id])
  end

  # POST /repeat_images
  # POST /repeat_images.json
  def create
    @repeat_image = RepeatImage.new(params[:repeat_image])

    respond_to do |format|
      if @repeat_image.save
        format.html { redirect_to @repeat_image, notice: 'Repeat image was successfully created.' }
        format.json { render json: @repeat_image, status: :created, location: @repeat_image }
      else
        format.html { render action: "new" }
        format.json { render json: @repeat_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /repeat_images/1
  # PUT /repeat_images/1.json
  def update
    @repeat_image = RepeatImage.find(params[:id])

    respond_to do |format|
      if @repeat_image.update_attributes(params[:repeat_image])
        format.html { redirect_to @repeat_image, notice: 'Repeat image was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @repeat_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /repeat_images/1
  # DELETE /repeat_images/1.json
  def destroy
    @repeat_image = RepeatImage.find(params[:id])
    @repeat_image.destroy

    respond_to do |format|
      format.html { redirect_to repeat_images_url }
      format.json { head :no_content }
    end
  end
end
