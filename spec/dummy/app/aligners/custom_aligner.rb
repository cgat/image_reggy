class CustomAligner < ImageReggy::Aligner::Base
  include ImageReggy::ROpenCV
  transform_type :srt
  control_point_generator :manual

  process :apply_transform
  process :crop_to_fit

end
