class NoCropAligner < ImageReggy::Aligner::Base
  include ImageReggy::ROpenCV
  transform_type :srt
  control_point_generator :manual

  process :apply_transform

end
