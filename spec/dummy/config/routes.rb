Rails.application.routes.draw do

  resources :historic_images


  resources :repeat_images


  mount ImageReggy::Engine => "/image_reggy"
end
