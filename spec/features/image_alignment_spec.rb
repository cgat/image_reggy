require 'spec_helper'

feature "Image Alignment" do
  let(:repeat_image) { FactoryGirl.create(:repeat_image) }
  let(:historic_image) { FactoryGirl.create(:historic_image) }
  before(:each) do
    repeat_image.historic_image = historic_image
    repeat_image.save!
    ImageReggy::AlignmentUploader.enable_processing = true
  end
  after(:each) do
    ImageReggy::AlignmentUploader.enable_processing = false
  end
  let(:points_initiator) do
    [{"x"=>"405", "y"=>"248"},
      {"x"=>"832", "y"=>"239"},
      {"x"=>"360", "y"=>"237"},
      {"x"=>"836", "y"=>"329"}]
  end
  let(:points_reference) do
    [ {"x"=>"224", "y"=>"104"},
      {"x"=>"1124", "y"=>"66"},
      {"x"=>"140", "y"=>"86"},
      {"x"=>"1134", "y"=>"248"} ]
  end

  scenario "two images from parent application are aligned", js: true do
    visit repeat_image_path(repeat_image)
    click_link("Align Images")
    page.should have_content("Set Control Points")
    page.should have_css('div#pointsable_initiator')
    page.should have_css('div#pointsable_reference')
    set_points("initiator", points_initiator)
    set_points("reference", points_reference)
    click_button "Align"
    page.should have_content("Verify Alignment")
    page.should have_link("Readjust & Try Again")
    page.should have_link("Done")
    #check that the classy compare class are inserted
    page.should have_css('.uc-mask')
    page.should have_css('.uc-bg')
    repeat_image_last_modified = File.mtime(repeat_image.image.file.path)
    historic_image_last_modified = File.mtime(historic_image.image.file.path)

    click_link "Done"

    page.should have_content("Repeat Image")
    #we expect that the reference image will be modified (overwriting)
    expect(File.mtime(historic_image.image.file.path)).to_not eq historic_image_last_modified
    #the initiator will be changed because of cropping
    expect(File.mtime(repeat_image.image.file.path)).to_not eq repeat_image_last_modified
    expect(MiniExiftool.new(historic_image.image.file.path).artist).to eq "Mountain Legacy Project"
    expect(MiniExiftool.new(repeat_image.image.file.path).copyright).to eq "Hasselblad H3D"

  end

  scenario "two images from parent application are aligned with no cropping", js: true do
    visit repeat_image_path(repeat_image)
    click_link("Align Images - No Crop")
    page.should have_content("Set Control Points")

    set_points("initiator", points_initiator)
    set_points("reference", points_reference)
    click_button "Align"

    repeat_image_last_modified = File.mtime(repeat_image.image.file.path)
    historic_image_last_modified = File.mtime(historic_image.image.file.path)
    click_link "Done"

    page.should have_content("Repeat Image")
    #we expect that the reference image will be modified (overwriting)
    expect(File.mtime(historic_image.image.file.path)).to_not eq historic_image_last_modified
    #the initiator will be changed because of cropping
    expect(File.mtime(repeat_image.image.file.path)).to eq repeat_image_last_modified
    expect(MiniExiftool.new(historic_image.image.file.path).artist).to eq "Mountain Legacy Project"
    expect(MiniExiftool.new(repeat_image.image.file.path).copyright).to eq "Hasselblad H3D"
  end

  scenario "control points are too linear", js: true do
    visit repeat_image_path(repeat_image)
    click_link("Align Images")
    page.should have_content("Set Control Points")

    set_points("initiator", [{'x' => 100,'y' => 100},{'x' => 200,'y' => 200},{'x' => 300,'y' => 300},{'x' => 400,'y' => 400}])
    set_points("reference", [{'x' => 100,'y' => 100},{'x' => 200,'y' => 200},{'x' => 300,'y' => 300},{'x' => 400,'y' => 400}])
    click_button "Align"

    page.should have_content("Set Control Points")
    page.should have_content("Error: Control points Failed to create transform. Control points should: be spread out and not lay in a line, "\
        "have a one to one correspondence between images, and have a minimum of 2 points per image.")
  end

  scenario 'when a warning is set and conditional true', js: true do
    CustomAligner.warning "This is a warning message", if: Proc.new { true }
    visit repeat_image_path(repeat_image)
    click_link("Align Images")
    page.should have_content("Set Control Points")
    set_points("initiator", points_initiator)
    set_points("reference", points_reference)
    click_button "Align"
    page.should have_content("Verify Alignment")
    page.should have_content("Warning")
    page.should have_content("This is a warning message")
  end

  scenario "cancelling process in the set_control_points page" do
    visit repeat_image_path(repeat_image)
    click_link("Align Images")
    click_link("Cancel")

    expect(ImageReggy::AlignmentImage.count).to eq 0
    expect(ImageReggy::ImagePair.count).to eq 0
  end

  scenario "cancelling process in the verify page" do
    visit repeat_image_path(repeat_image)
    click_link("Align Images")
    visit image_reggy.image_pair_alignment_path(1, :verify_alignment)
    click_link("Cancel")

    expect(ImageReggy::AlignmentImage.count).to eq 0
    expect(ImageReggy::ImagePair.count).to eq 0
  end

end

def set_points(container_name, points)
  points.each_with_index do |p, index|
    page.execute_script("window['pointsable_#{container_name}'].movePoint(#{index}, {x: #{p['x']}, y: #{p['y']}}, 'full_image');")
  end
end
