FactoryGirl.define do
  factory :points, :class => "Pointsable::Points" do
    sequence :x do |n|
      n*10
    end
    sequence :y do |n|
      n*10+10
    end
    sequence :label do |n|
      "p#{n}"
    end
  end
end
