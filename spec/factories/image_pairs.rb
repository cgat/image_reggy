# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :image_pair, :class => 'ImageReggy::ImagePair' do
    association :initiator, factory: :initiator_with_associations
    association :reference, factory: :reference_with_associations
    trait :without_points do
      association :initiator, factory: :initiator_with_associations_no_points
      association :reference, factory: :reference_with_associations_no_points
    end
    aligner "CustomAligner"
  end
end
