include ActionDispatch::TestProcess #include so that we can use the { fixture_file_upload with carrierwave uploads

FactoryGirl.define do
  factory :repeat_image, :class => 'RepeatImage' do
    image {fixture_file_upload(File.join('spec','photos','image_initiator.jpg')) }
  end
end
