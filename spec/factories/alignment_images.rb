# Read about factories at https://github.com/thoughtbot/factory_girl
include ActionDispatch::TestProcess #include so that we can use the { fixture_file_upload with carrierwave uploads

FactoryGirl.define do
  factory :alignment_image, :class => 'ImageReggy::AlignmentImage' do
    trait :with_initiator_image do
      image {fixture_file_upload(File.join('spec','photos','image_initiator.jpg')) }
    end
    trait :with_reference_image do
      image {fixture_file_upload(File.join('spec','photos','image_reference.jpg')) }
    end
    trait :with_points do
      transient do
        num_of_points 5
      end
      after(:create) do |this_image, evaluator|
        FactoryGirl.create_list(:points, evaluator.num_of_points, pointsable: this_image)
      end
    end
    trait :with_initiator_derivative do
      derivative {FactoryGirl.create(:repeat_image)}
    end
    trait :with_reference_derivative do
      derivative {FactoryGirl.create(:historic_image)}
    end
    image_meta OpenStruct.new(image_width: 2000, image_height: 1500)
    derivative_id "MyString"
    derivative_type "MyString"
    has_changed false
    factory :initiator_with_associations, traits: [:with_points, :with_initiator_image, :with_initiator_derivative]
    factory :reference_with_associations, traits: [:with_points, :with_reference_image, :with_reference_derivative]
    factory :initiator_with_associations_no_points, traits: [:with_initiator_image, :with_initiator_derivative]
    factory :reference_with_associations_no_points, traits: [:with_reference_image, :with_reference_derivative]
  end
end
