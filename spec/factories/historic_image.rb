include ActionDispatch::TestProcess #include so that we can use the { fixture_file_upload with carrierwave uploads

FactoryGirl.define do
  factory :historic_image, :class => 'HistoricImage' do
    image {fixture_file_upload(File.join('spec','photos','image_reference.jpg')) }
  end
end
