require 'spec_helper'

class BasicAligner < ImageReggy::Aligner::Base
  transform_type :srt
end

describe ImageReggy::ImagePairForm do
  let(:points_attributes) do
      {"points_attributes"=>
        {"1391431488211"=>
          {"x"=>"280", "y"=>"200", "label"=>"p1", "_destroy"=>"false"},
         "1391431488217"=>
          {"x"=>"560", "y"=>"200", "label"=>"p2", "_destroy"=>"false"},
         "1391431488223"=>
          {"x"=>"840", "y"=>"200", "label"=>"p3", "_destroy"=>"false"},
         "1391431488229"=>
          {"x"=>"1120", "y"=>"200", "label"=>"p4", "_destroy"=>"false"}}}
    end

  describe '#create' do

    let(:initiator_derivative) { FactoryGirl.create(:repeat_image) }
    let(:initiator_params) { {initiator_derivative_class: initiator_derivative.class.name, initiator_derivative_id: initiator_derivative.id } }
    let(:reference_derivative) { FactoryGirl.create(:historic_image) }
    let(:reference_params) { {reference_derivative_class: reference_derivative.class.name, reference_derivative_id: reference_derivative.id } }
    let(:aligner) { "BasicAligner" }
    let(:my_params) { initiator_params.merge(reference_params).merge({aligner: aligner}) }
    let(:ip_form) { ImageReggy::ImagePairForm.new() }

    it 'should create alignment images for the image pair' do
      ip_form.create(my_params)
      expect(ip_form.initiator).to be_present
      expect(ip_form.reference).to be_present
    end

    it 'should persist the aligner class within the image pair' do
      ip_form.create(my_params)
      expect(ip_form.image_pair.aligner).to eq "BasicAligner"
    end

    it 'should be able to accept points for the initiator image from the derivative' do
      params = my_params.merge({image_initiator: points_attributes})
      ip_form.create(params)
      expect(ip_form.initiator.points.count).to eq 4
    end

    it 'should be able to accept points for the reference image from the derivative' do
      params = my_params.merge({image_reference: points_attributes})
      ip_form.create(params)
      expect(ip_form.reference.points.count).to eq 4
    end

    it 'should fail if the aligner class does not exist' do
      my_params = initiator_params.merge(reference_params).merge({aligner: "DoesNotExistAligner"})
      expect(ip_form.create(my_params)).to be_falsey
    end

    it 'should fail if a derivative objects is not given' do
      my_params = initiator_params.merge({aligner: aligner})
      expect(ip_form.create(my_params)).to be_falsey
    end

  end

  describe '#update' do

    it 'should raise an error if the image_pair was not initalized in the initalizer' do
      expect{ImageReggy::ImagePairForm.new().update(nil)}.to raise_error StandardError, /#update cannot be executed/
    end

    it 'should update the points of the initiator' do
      image_pair = FactoryGirl.create(:image_pair, :without_points)
      ip_form = ImageReggy::ImagePairForm.new(image_pair.id)
      ip_form.update({image_initiator: points_attributes, image_reference: {}})
      expect(ip_form.initiator.reload.points.count).to eq 4
    end

    it 'should update the points of the reference' do
      image_pair = FactoryGirl.create(:image_pair, :without_points)
      ip_form = ImageReggy::ImagePairForm.new(image_pair.id)
      ip_form.update({image_reference: points_attributes, image_initiator: {}})
      expect(ip_form.reference.reload.points.count).to eq 4
    end

  end

end
