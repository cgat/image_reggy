require 'spec_helper'

describe ImageReggy::AlignmentUploader do

  before(:each) do
    ImageReggy::AlignmentUploader.enable_processing = true
  end

  let(:derivative) do
    ri = FactoryGirl.build(:repeat_image)
    ri.save!
    ri
  end

  let(:uploader) do
    ai = ImageReggy::AlignmentImage.new
    ai.image = File.open(derivative.image.file.path)
    ai.derivative = derivative
    ai.save!
    ai.image
  end

  after(:each) do
    ImageReggy::AlignmentUploader.enable_processing = false
    uploader.remove!
  end

  it "should have a large version named appropriately" do
    derivative_path = derivative.image.file.path
    base_filename = File.basename(derivative_path).chomp(File.extname(derivative_path))
    expect(File.basename(uploader.large.file.path)).to eq "large_#{base_filename}.jpeg"
  end

  it "should have a web version named appropriately" do
    derivative_path = derivative.image.file.path
    base_filename = File.basename(derivative_path).chomp(File.extname(derivative_path))
    expect(File.basename(uploader.web.file.path)).to eq "web_#{base_filename}.jpeg"
  end

  context "tiff image" do

    let(:derivative) do
      ri = FactoryGirl.build(:repeat_image, image: File.open(File.join("spec", "photos", "color_8bit.tif")))
      ri.save!
      ri
    end

    it "should have a large version named appropriately" do
      derivative_path = derivative.image.file.path
      base_filename = File.basename(derivative_path).chomp(File.extname(derivative_path))
      expect(File.basename(uploader.large.file.path)).to eq "large_#{base_filename}.jpeg"
    end

    it "should have a web version named appropriately" do
      derivative_path = derivative.image.file.path
      base_filename = File.basename(derivative_path).chomp(File.extname(derivative_path))
      expect(File.basename(uploader.web.file.path)).to eq "web_#{base_filename}.jpeg"
    end
  end
end
