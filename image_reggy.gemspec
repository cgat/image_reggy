$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "image_reggy/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "image_reggy"
  s.version     = ImageReggy::VERSION
  s.authors     = ["Chris Gat"]
  s.email       = ["chris.gat@gmail.com"]
  s.homepage    = "https://bitbucket.org/cgat/image_reggy"
  s.summary     = "An image registration engine for Rails apps."
  s.description = "This engine provides functionality to manually register images from a parent application using a set"+\
  " of corresponding points between two images. Currently the only transform supported is ScaleRotationTranslate "+\
  "(affine with uniform scaling). At the moment, it is assumed that the parent application uses carrierwave for store images."+\
  " ImageReggy also requires use of a local file system."

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", "~> 4.1"
  s.add_dependency 'carrierwave', '~>0.10'
  s.add_dependency 'carrierwave-meta'
  s.add_dependency 'ruby-vips', '~>0.3.14'
  s.add_dependency 'carrierwave-vips'
  s.add_dependency 'wicked'
  s.add_dependency "jquery-rails"
  s.add_dependency 'pointsable'
  s.add_dependency 'ropencv', "~> 0.0.22"
  s.add_dependency 'mini_exiftool', "~> 2.4.0"
  s.add_dependency 'protected_attributes'


  s.add_development_dependency "sqlite3"
  s.add_development_dependency 'rspec-rails', '2.99'
  s.add_development_dependency 'capybara'
  s.add_development_dependency 'factory_girl_rails'
  s.add_development_dependency 'selenium-webdriver'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'capybara-firebug'
end
