# encoding: utf-8

require 'image_reggy/aligner/processing'
require 'image_reggy/aligner/configuration'
require 'image_reggy/aligner/control_points'
require 'image_reggy/aligner/exporter'
require 'image_reggy/aligner/metadata'
require 'image_reggy/aligner/warning'
require 'image_reggy/processing/ropencv'

module ImageReggy
  module Aligner

    class Base
      def initialize(image_pair, version=:full)
        raise StandardError, "The Aligner class you are using doesn't have an alignment processor included" + \
        " or the processor that is included does not implement the transform method" unless self.respond_to?(:transform!)
        @image_pair = image_pair
        @version = version
        @initiator = image_pair.initiator
        @reference = image_pair.reference
      end
      include ImageReggy::Aligner::Processing
      include ImageReggy::Aligner::Configuration
      include ImageReggy::Aligner::ControlPoints
      include ImageReggy::Aligner::Exporter
      include ImageReggy::Aligner::Metadata
      include ImageReggy::Aligner::Warning
    end

  end
end
