module ImageReggy
  module Aligner
    module Exporter

      def export(image_pair)
        initiator = image_pair.initiator
        reference = image_pair.reference
        initiator_derivative = initiator.derivative
        reference_derivative = reference.derivative
        if initiator.has_changed?
          overwrite_derivative_image(initiator, initiator_derivative)
        end
        if reference.has_changed?
          overwrite_derivative_image(reference, reference_derivative)
        end
        true
      end

      private

      def overwrite_derivative_image(alignment_image, derivative)
        FileUtils.mv(alignment_image.image.file.path, derivative.image.file.path, force: true)
        derivative.image.recreate_versions!
      end
    end
  end
end

