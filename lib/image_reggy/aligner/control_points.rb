
module ImageReggy
  module Aligner
    module ControlPoints
      extend ActiveSupport::Concern

      included do
        class_attribute :_control_point_generator, :instance_writer => false
        self._control_point_generator = :manual

      end

      module ClassMethods

        def control_point_generator(generator=nil)
          generator = generator.to_sym if generator.respond_to?(:to_sym)
          raise ArgumentError, "Hash arguments must contain a single key/value pair" if generator.is_a?(Hash) && generator.count!=1

          if generator
            self._control_point_generator=generator
          end
          self._control_point_generator
        end

      end #ClassMethods

      def set_control_points!
        case self._control_point_generator
        when :manual then false
        when Symbol then self.send(self._control_point_generator)
        when Hash then self.send(self._control_point_generator.first[0], *self._control_point_generator.first[1])
        else raise ArgumentError, "Invalid control_point_generator: #{self._control_point_generator}"
        end
      end

    end
  end
end
