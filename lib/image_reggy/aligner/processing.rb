
module ImageReggy
  module Aligner
    module Processing
      extend ActiveSupport::Concern

      included do
        class_attribute :processors, :instance_writer => false
        self.processors = []

      end

      module ClassMethods
        def process(*args)
          if !args.first.is_a?(Hash) && args.last.is_a?(Hash)
            conditions = args.pop
            args.map!{ |arg| {arg => []}.merge(conditions) }
          end

          args.each do |arg|
            if arg.is_a?(Hash)
              condition = arg.delete(:if)
              arg.each do |method, args|
                self.processors += [[method, args, condition]]
              end
            else
              self.processors += [[arg, [], nil]]
            end
          end
        end
      end #ClassMethods

      def align!
        self.transform!
        self.class.processors.each do |method, args, condition|
          if(condition)
            next if !(condition.respond_to?(:call) ? condition.call(self, :args => args, :method => method) : self.send(condition))
          end
          self.send(method, *args)
        end
        self.save_images!

        #the initiator and reference has_changed methods are only available
        #with :web version (so we don't eat up memory)
        if @version==:web
          @initiator.update_attribute(:has_changed, initiator_has_changed?)
          @reference.update_attribute(:has_changed, reference_has_changed?)
        end

        #we only want to save metadata on the full versions.
        if @version==:full
          if self.save_metadata?
            self.save_metadata(@initiator.derivative.image.file.path, @initiator.image.file.path)
            self.save_metadata(@reference.derivative.image.file.path, @reference.image.file.path)
          end
        end
      end

    end
  end
end
