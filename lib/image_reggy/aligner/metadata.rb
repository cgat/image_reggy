module ImageReggy
  module Aligner
    module Metadata

      def save_metadata(src_path, dst_path, tags=['all'])
        tags << 'icc_profile' if save_icc_profile? && !tags.include?('icc_profile')
        meta = MiniExiftool.new(dst_path)
        meta.copy_tags_from(src_path, tags)
      end

      def save_metadata?
        true
      end

      def save_icc_profile?
        true
      end

    end
  end
end
