module ImageReggy
  module Aligner
    module Warning
      extend ActiveSupport::Concern

      included do
        class_attribute :warnings, :instance_writer => false
        self.warnings = []

      end

      module ClassMethods
        def warning(msg, condition)
          condition = condition.is_a?(Hash) && condition.has_key?(:if) ? condition[:if] : condition
          self.warnings += [[msg, condition]]
        end
      end #ClassMethods

      def warning_messages?
        warnings.any?{|w| w.last.respond_to?(:call) ?  w.last.call : send(w.last) }
      end

      def warning_messages
        warnings.select{|w|w.last.respond_to?(:call) ?  w.last.call : send(w.last) }.map{|w| w.first }
      end

    end
  end
end

