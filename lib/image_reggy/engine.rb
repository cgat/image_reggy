require 'carrierwave'
require 'carrierwave-vips'
require 'carrierwave-meta'
require 'wicked'
require "jquery-rails"
require "mini_exiftool"

module ImageReggy
  class Engine < ::Rails::Engine
    isolate_namespace ImageReggy
    config.generators do |g|
      g.test_framework      :rspec,        :fixture => false
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
      g.assets false
      g.helper false
    end
  end
end
