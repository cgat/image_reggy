
module ImageReggy
  module ROpenCV
    extend ActiveSupport::Concern
    IMREAD_UNCHANGED = -1
    included do
      begin
        require 'ropencv'
        include OpenCV
        RectContainer.send(:include, OpenCV)
      rescue LoadError => e
        e.message << " (You may need to install the ropencv gem)"
        raise e
      end
      attr_reader :cv_img_init, :cv_img_ref, :t_initiator, :t_reference
    end

    def transform!
      case transform_type
      when :srt
        transform_affine(false)
      when :affine
        transform_affine(true)
      end
    end

    def transform_affine(fullAffine=false)
      manipulate_transforms! do |transform1, transform2|
        points1 = points_array_to_vector(@initiator.scaled_coords(@version))
        points2 = points_array_to_vector(@reference.scaled_coords(@version))
        #we want the transform from the reference to the initiator
        transform2 = cv::estimate_rigid_transform(points2, points1, fullAffine)
        raise UnableToTransformError, "Failed to create transform. Control points should: be spread out and not lay in a line, "\
        "have a one to one correspondence between images, and have a minimum of 2 points per image." if transform2.to_a.blank?
        transform1 = cv::Mat::eye(2,3, cv::CV_32F)
        [transform1, transform2]
      end
    end

    def apply_transform
      manipulate! do |img_mat1, img_mat2, transform1, transform2|
        viewport_size = img_mat1.size
        #Once the transform is applied, the size of the original reference mat is lost
        #We need this size to determine a proper cropping of the result, so save it
        #in instance variable (see crop_to_fit for use)
        @ref_size_before_warp = img_mat2.size

        create_dest = lambda {|type|cv::Mat::zeros(viewport_size.height, viewport_size.width, type) }

        dest = create_dest.call(img_mat1.type) and cv::warp_affine(img_mat1, dest, transform1, viewport_size)
        img_mat1 = dest

        dest = create_dest.call(img_mat2.type) and cv::warp_affine(img_mat2, dest, transform2, viewport_size)
        img_mat2 = dest

        [img_mat1, img_mat2, transform1, transform2]
      end
    end

    def crop(start_x, start_y, width, height)
      if @cv_img_init.size.width!=@cv_img_ref.size.width || @cv_img_init.size.height!=@cv_img_ref.size.height
        raise StandardError, "Both images in the image pair have to have the same dimensions to use #crop. Make sure to call #apply_transform first."
      end
      manipulate_images! do |img_mat1, img_mat2|
        rect = cv::Rect.new(start_x, start_y, width, height)
        img_mat1 = img_mat1.block(rect)
        img_mat2 = img_mat2.block(rect)
        [img_mat1, img_mat2]
      end
    end

    #crop_to_fit is an intelligent crop, that will find the best cropping based on the
    #way the images have been transformed.
    def crop_to_fit
      if @cv_img_init.size.width!=@cv_img_ref.size.width || @cv_img_init.size.height!=@cv_img_ref.size.height
        raise StandardError, "Both images in the image pair have to have the same dimensions to use #crop_to_fit. Make sure to call #apply_transform first."
      end
      manipulate! do |img_mat1, img_mat2, transform1, transform2|
        rect_container = RectContainer.new(img_mat2.size.width,img_mat2.size.height, @ref_size_before_warp.width, @ref_size_before_warp.height, transform2)
        if rect_container.changed?
          rect = cv::Rect.new(rect_container.top_left[0], rect_container.top_left[1], rect_container.width, rect_container.height)
          img_mat1 = img_mat1.block(rect)
          img_mat2 = img_mat2.block(rect)
        end
        [img_mat1, img_mat2, transform1, transform2]
      end
    end

    def manipulate_images!
      load_images
      @cv_img_init, @cv_img_ref = yield @cv_img_init, @cv_img_ref
      rescue => e
        raise StandardError.new("Failed to manipulate images. Original Error: #{e}")
    end

    def manipulate_transforms!
      @t_initiator, @t_reference = yield @t_initiator, @t_reference
    rescue UnableToTransformError => e
      raise e
    rescue => e
      raise StandardError.new("Failed to manipulate_transforms. Original Error: #{e}")
    end

    def manipulate!
      load_images
      @cv_img_init, @cv_img_ref, @t_initiator, @t_reference = yield @cv_img_init, @cv_img_ref, @t_initiator, @t_reference
    rescue => e
      raise StandardError.new("Failed to manipulate. Original Error: #{e}")
    end

    def save_images!
      cv::imwrite(@initiator.image_path(@version), @cv_img_init)
      cv::imwrite(@reference.image_path(@version), @cv_img_ref)
    end

    def load_images
      @cv_img_init ||= cv::imread(@initiator.image_path(@version).to_s, IMREAD_UNCHANGED)
      @cv_img_ref ||= cv::imread(@reference.image_path(@version).to_s, IMREAD_UNCHANGED)
      if (@version==:web)
        #create tmp copies of the original images so we can determine if the original has changed.
        #only do this for web version because full version may be a very large image and the duplication
        #would eat up memory
        @tmp_img_init ||= cv::imread(@initiator.image_path(@version).to_s, IMREAD_UNCHANGED)
        @tmp_img_ref ||= cv::imread(@reference.image_path(@version).to_s, IMREAD_UNCHANGED)
      end
    end

    def mat_difference?(mat1, mat2)
      if mat1.size.width == mat2.size.width && mat1.size.height == mat2.size.height && mat1.type == mat2.type
        mat1.to_a!=mat2.to_a
      else
        true
      end
    end

    def initiator_has_changed?
      raise StandardError, "This method can only be used when the :web version is being processed. Current version: #{@version}" if @version!=:web
      mat_difference?(@tmp_img_init, @cv_img_init)
    end

    def reference_has_changed?
      raise StandardError, "This method can only be used when the :web version is being processed. Current version: #{@version}" if @version!=:web
      mat_difference?(@tmp_img_ref, @cv_img_ref)
    end


    def hash_images
      load_images
      [Digest::MD5.hexdigest(@cv_img_init.data.read_string), Digest::MD5.hexdigest(@cv_img_ref.data.read_string)]
    end

    module Helper

      def extract_srt(transform)
        raise StandardError, "#extract_srt does not support transform_type #{transform_type}" if transform_type!=:srt
        a = transform[1,0]
        c = transform[0,0]
        translate_x = transform[0,2]
        translate_y = transform[1,2]
        scale = Math.sqrt(a*a+c*c)
        rotation = Math.atan2(a,c)
        return [scale,rotation,[translate_x,translate_y]]
      end

      def make_srt_transform(scale, rotation, translate, rotation_origin=[0,0])
        raise StandardError, "#make_srt_transform does not support transform_type #{transform_type}" if transform_type!=:srt
        transform=cv::get_rotation_matrix2d(cv::Point2f.new(rotation_origin[0],rotation_origin[1]), rotation, scale)
        transform[0,2]=transform[0,2]+translate[0]
        transform[1,2]=transform[1,2]+translate[1]
        return transform
      end

      def transform_points(points, transform)
        new_points = cv::Mat.new(points.count,2,cv::CV_32F)
        cv::transform(points_array_to_vector(points),new_points, transform)
        new_points.to_a
      end

      def points_array_to_vector(points_array, type=cv::Point2f)
        vector = std::Vector.new(type)
        points_array.each{ |p| vector<<type.new(p[0],p[1]) }
        vector
      end

      class RectContainer
        include Helper

        def initialize(canvas_width, canvas_height, previous_width, previous_height,  transform)
          @canvas_width = canvas_width
          @canvas_height = canvas_height
          @transform = transform
          corners = [[0,0],[previous_width-1, 0], [previous_width-1, previous_height-1], [0, previous_height-1]]
          @t_top_left, @t_top_right, @t_bottom_right, @t_bottom_left = transform_points(corners, transform)
        end

        def top_left
          [min_x(points_array), min_y(points_array)]
        end

        def top_right
          [max_x(points_array), min_y(points_array)]
        end

        def bottom_right
          [max_x(points_array), max_y(points_array)]
        end

        def bottom_left
          [min_x(points_array), max_y(points_array)]
        end

        def width
          #coord index starts at zero, so add 1 for width
          top_right[0]-top_left[0]+1
        end

        def height
          bottom_left[1]-top_left[1]+1
        end

        def changed?
         !( top_left[0]==0 && top_left[1]==0 && \
          top_right[0]==(@canvas_width-1) && top_right[1]==0 && \
          bottom_right[0]==(@canvas_width-1) && bottom_right[1]==(@canvas_height-1) && \
          bottom_left[0]==0 && bottom_left[1]==(@canvas_height-1))
        end

        private

        def inbounds_x_axis?(val)
          val>=0 && val<@canvas_width
        end

        def inbounds_y_axis?(val)
          val>=0 && val<@canvas_height
        end

        def cvmat_to_point_array(mat)
          return mat if mat.is_a?(Array)
          [mat[0][0], mat[0][1]]
        end

        def points_array
          [@t_top_left, @t_top_right, @t_bottom_right, @t_bottom_left]
        end

        def min_x(points_array)
          points_array = Array(points_array)
          val = points_array.sort{|a,b| a[0]<=>b[0]}.first[0]
          inbounds_x_axis?(val) ? val : 0
        end

        def max_x(points_array)
          points_array = Array(points_array)
          val = points_array.sort{|a,b| a[0]<=>b[0]}.last[0]
          inbounds_x_axis?(val) ? val : @canvas_width-1
        end

        def max_y(points_array)
          points_array = Array(points_array)
          val = points_array.sort{|a,b| a[1]<=>b[1]}.last[1]
          inbounds_y_axis?(val) ? val : @canvas_height-1
        end

        def min_y(points_array)
          points_array = Array(points_array)
          val = points_array.sort{|a,b| a[1]<=>b[1]}.first[1]
          inbounds_y_axis?(val) ? val : 0
        end

      end

    end
    include Helper
  end
end


