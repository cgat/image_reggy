require "image_reggy/engine"
require 'active_support/concern'

module ImageReggy
  class Railtie < Rails::Railtie
  end
end

require 'image_reggy/aligner'
require 'image_reggy/error'
